source ~/.aliases.rc.common
source ~/.environmental.rc.common
# aws completion
autoload bashcompinit && bashcompinit
autoload -Uz compinit && compinit
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=20000
SAVEHIST=20000
setopt HIST_FIND_NO_DUPS
# following should be turned off, if sharing history via setopt SHARE_HISTORY
#setopt INC_APPEND_HISTORY

setopt SHARE_HISTORY 



autoload -U compinit
compinit
autoload promptinit
promptinit
autoload -U colors && colors
setopt appendhistory extendedglob nomatch notify
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/larry/.zshrc'

autoload -Uz compinit
compinit

#geometry on ubuntu gnome doesn't seem to go full screen but this is better format is width highth then width offset and highth offset for position 

# End of lines added by compinstall
#
#
export ORACLE_HOME=/usr/lib/oracle/12.1/client64
export LD_LIBRARY_PATH="$ORACLE_HOME/lib"
export PATH="$ORACLE_HOME:$PATH"
export PATH="/home/larry/code/utilities:$PATH"
export GOPATH="/home/larry/Go"
export GOROOT="/usr/lib/golang"
export PASSWORD_STORE_ENABLE_EXTENSIONS="true"
[[ "$COLORTERM" == (24bit|truecolor) || "${terminfo[colors]}" -eq '16777216' ]] || zmodload zsh/nearcolor

PROMPT="%{%}[%M %D %T]%n@%d%#%{%}"
COLOR="red"



# Get color support for 'less'
export LESS="--RAW-CONTROL-CHARS"

# Use colors for less, man, etc.
[[ -f ~/.LESS_TERMCAP ]] && . ~/.LESS_TERMCAP
export GROFF_NO_SGR=1
